# Instructions:
Read the entire instructions before getting started. Checkout or download the archive to your computer/virtual machine/docker instance. This project requires npm, git and bower. Make sure these are installed.

Remember to run ```npm install``` and ```bower install``` after checking out.

Develop this simple AngularJS (1.x) application:


1.Create a directive or component that will display the multi-bar chart provided in ```src/charts```. It may refer to the ```barMultiVertical2``` function or wrap it directly.


2.Create a service that loads the data from ```data/data.json```. This would be a service that would use the angular $http service. Use promises.


3.Below the chart, create a dropdown single select (directive or component) with two options (**City** and **Quarter**). It should swap the x and z axes, therefore changing the grouping from cities to quarters.

  **City** will show cities on the bottom axis and quarters in the right side legend.

  **Quarter** will show quarters on the bottom axis and cities in the side legend.

  This dropdown should communicate with the chart via HTML attributes.

_Do **NOT** use a 3rd party library for this dropdown. Build the template and code it yourself._

## Use Git
You must use .git to track your progress. Commit changes as if you were member of a team. Use reasonable descriptive commit messages.

## Use comments
Document your code with reasonable comments.

## 3rd party libraries
The only libraries allowed are the ones provided in ```package.json``` and ```bower.json```, except where otherwise specifically mentioned.

# Before you submit your work
Provide us with a .zip archive containing the repository with your work along with a history of commits. Alternatively you may upload the repo to a 3rd party repository (github, bitbucket, etc..). Provide us with credentials if needed.

Include instructions on how to install/run.

# Bonus
  If you can, do any number of these additional tasks in any order.

  * Concatenate and minify all the .js dependencies, so that ```main.js``` is the only script tag. You may use a 3rd party library/tool for this (grunt/gulp, etc..)
  * The data JSON comes with a title. Put this title into the SVG chart as a <text> element at the top. Allow the other elements in the chart to be displayed properly without overlapping the title.
  * Create an HTML table with the data provided below the dropdown. Display all the data (structure is up to you) and allow the user to sort the table by clicking the column headers. Clicking the same header should reverse sort. Indicate which column is being sorted and sort direction. You may do this in any way that you deem appropriate.
  * Create some unit tests with Karma for your angular code.
  * Style the application using Sass.