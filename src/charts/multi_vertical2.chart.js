/*Give data in the following format:
{
	"name": "Quartely Top Destinations",
	"data": [{
		"x": "Q1",
    "y": 18000,
    "z": "LA",
	},{
		"x": "Q2",
    "y": 19000,
    "z": "LA",
	},{
		"x": "Q3",
    "y": 20000,
    "z": "LA",
	},{
		"x": "Q4",
    "y": 21000,
    "z": "LA",
	},{
		"x": "Q1",
    "y": 11000,
    "z": "NY",
	},{
		"x": "Q2",
    "y": 12000,
    "z": "NY",
	},{
		"x": "Q3",
    "y": 14000,
    "z": "NY",
	},{
		"x": "Q4",
    "y": 13000,
    "z": "NY",
	},{
		"x": "Q1",
    "y": 39000,
    "z": "SF",
	},{
		"x": "Q2",
    "y": 23000,
    "z": "SF",
	},{
		"x": "Q3",
    "y": 33000,
    "z": "SF",
	},{
		"x": "Q4",
    "y": 34000,
    "z": "SF",
	}],
	"units": "$" //optional
}*/

function barMultiVertical2() {
  var data = [];
  var refresh;
  var config = {
    colors: ['#FF71D4','#5AAAFA']
    ,transitions : false
    ,transitionDuration : 500
    ,width : 500
    ,height : 300
  };

  function chart(selection) {
    selection.each(function() {
      var graphDimensions  = {};
      var legendDimensions = {};
      var fontSize = {};
      var parentRowScale, childRowScale, columnScale, legendScale, colors;

      var dom = d3.select(this);
      var svg = dom.append('svg')
        .attr('class', 'bar-chart')
        .attr('height', config.height)
        .attr('width', config.width);

      //single refresh function
      var groupData;
      refresh = function() {
        //recalc dimensions
        svg.attr('width', config.width);
        svg.attr('height', config.height);

        if(data == undefined || data.data == undefined) return;
        fontSize.bottom = Math.ceil(config.height*0.04);
        fontSize.legend = config.height*0.04;
        fontSize.top = Math.ceil(config.height * 0.04);
        graphDimensions.top = 0 + fontSize.top * 2;
        graphDimensions.bottom = config.height - fontSize.bottom*3;
        graphDimensions.left = config.width * 0.02;
        graphDimensions.right = config.width*0.8;
        graphDimensions.width = graphDimensions.right - graphDimensions.left;
        graphDimensions.height = graphDimensions.bottom - graphDimensions.top;
        legendDimensions.top = 0;
        legendDimensions.bottom = fontSize.legend * 3;
        legendDimensions.margin = config.width*.05;
        legendDimensions.left = graphDimensions.right + legendDimensions.margin;
        legendDimensions.right = config.width;
        legendDimensions.height = legendDimensions.bottom - legendDimensions.top;
        legendDimensions.width = config.width - graphDimensions.width - legendDimensions.margin;
        legendDimensions.rectWidth = legendDimensions.width / 5;
        legendDimensions.rectHeight = legendDimensions.rectWidth / 2;

        //recalc scales
        parentRowScale = d3.scaleBand().round(true).range([graphDimensions.left, graphDimensions.right]).paddingInner(0.5)
          .domain(data.data.map(function(d){ return d.z; }));
        columnScale = d3.scaleLinear().range([0, graphDimensions.height])
          .domain([0, d3.max(data.data.map(function(d){ return d.y; }))]);
        childRowScale = d3.scaleBand().round(true).range([0, parentRowScale.bandwidth()]).paddingInner(0.2)
          .domain(data.data.map(function(d){ return d.x; }));

        //group data by z
        groupData = d3.nest().key(function(d){ return d.z; }).entries(data.data);

        legendScale = d3.scaleBand().round(true).range([legendDimensions.top, legendDimensions.bottom], .1)
          .domain(groupData[0].values.map(function(d){return d.x;}));
        colors = d3.scaleOrdinal().range(config.colors);
        colors.domain(groupData[0].values.map(function(d){return d.x;}));

        //delete top-labels
        svg.selectAll('text.top-label').remove();

        //draw groups
        var groups = svg.selectAll('g.parent-row').data(groupData);
        groups.call(arrangeParentRow);
        groups.enter().append('g').call(arrangeParentRow);
        groups.exit().remove();

        //draw bottom legend
        var bottomLabels = svg.selectAll('text.parent-legend').data(groupData);
        bottomLabels.call(arrangeParentRowLegend);
        bottomLabels.enter().append('text').call(arrangeParentRowLegend).attr('class', 'parent-legend');
        bottomLabels.exit().remove();

        var legendConfig = {
          dimensions: legendDimensions,
          direction: 'vertical',
          colors: colors,
          rootElement: svg,
          class:"legend-item",
          fontSize: fontSize.legend,
          distribution: 'bunched'
        }
        ctLegendMaker(groupData[0].values.map(function(d){return d.x}), legendConfig);
      }

      function arrangeParentRow(selection) {
        selection.each(function(d, i) {
          var index = i;
          var elements = d3.select(this).attr('class', 'parent-row');
          var childRow = elements.selectAll("rect.col").data(d.values);
          childRow.call(arrangeBars);
          childRow.enter().append('rect').call(arrangeBars).attr('class', 'col');
          childRow.exit().remove();
        });
      }

      function arrangeBars(selection) {
        selection.each(function(d, i) {
          var index = i;
          var elements = d3.select(this)
          .attr('x', function(d){ return childRowScale(d.x) + parentRowScale(d.z);})
          .attr('y', function(d){ return graphDimensions.bottom - columnScale(d.y) })
          .attr('width', childRowScale.bandwidth())
          .attr('height', function(d) { return columnScale(d.y) })
          .attr('fill', function(d){ return colors(d.x)});

          var topLabel = svg.append('text')
          .attr('class','top-label')
          .attr('font-size',fontSize.top+'px')
          .attr('text-anchor', 'middle')
          .attr('x', childRowScale(d.x) + childRowScale.bandwidth()/2 + parentRowScale(d.z))
          .attr('y', graphDimensions.bottom - columnScale(d.y) - fontSize.top*1/3)
          .text(d.y);
        });
      }

      function arrangeParentRowLegend(selection) {
        selection.each(function(d, i) {
          var index = i;
          var elements = d3.select(this);
          elements.text(d.key)
          .attr('x', function(d){ return parentRowScale(d.key) + parentRowScale.bandwidth()/2; })
          .attr('y', (graphDimensions.bottom + fontSize.bottom*1.5) + fontSize.bottom*(index%2))
          .attr('font-size', fontSize.bottom+'px')
          .attr('text-anchor', 'middle');
        });
      }
    });
  }

  chart.config = function(value) {
    if (!arguments.length) return config;
    _.assign(config, value);
    return chart;
  }

  chart.data = function(value) {
    if (!arguments.length) return data;
    data = value;
    return chart;
  }

  chart.refresh = function() {
    if (typeof refresh === 'function') refresh();
    return chart;
  }
  return chart;
}

function ctLegendMaker(data, config) {
  config = config || {
    dimensions: {
      top: 0,
      bottom: 500,
      left: 0,
      right: 200
    },
    direction: 'vertical',
    colors: d3.schemeCategory20,
    rootElement: undefined,
    class:"legend-item",
    fontSize: 12,
    transitionDuration: 0,
    distribution: 'even',
  }

  //assign defaults
  config = config || {};
  config.dimensions = config.dimensions || {};
  config.dimensions.top = config.dimensions.top || 0;
  config.dimensions.bottom = config.dimensions.bottom || 500;
  config.dimensions.left = config.dimensions.left || 0;
  config.dimensions.right = config.dimensions.right || 200;
  config.direction = config.direction || 'vertical';
  config.colors = config.colors || d3.scale.category20c();
  if (config.rootElement == undefined) {throw "Root element must be defined"}
  config.class = config.class || "legend-item";
  config.fontSize = config.fontSize || 12;
  config.transitionDuration = config.transitionDuration || 0;
  config.distribution = config.distribution || 'even';

  config.dimensions.width = config.dimensions.right - config.dimensions.left;
  config.dimensions.height = config.dimensions.bottom - config.dimensions.top;

  var legendScale;
  if(config.distribution == 'bunched') {
    legendScale = function(val) {
      return data.indexOf(val) * config.fontSize*4;
    }
  } else {
    legendScale = d3.scale.ordinal().rangeRoundPoints([config.dimensions.top, config.dimensions.bottom], .1).domain(data);
  }
  config.colors.domain(data);

  var rectWidth = config.fontSize * 3;
  var rectHeight = config.fontSize * 1.5;

  var legendItems = config.rootElement.selectAll('g.legend-item').data(data);
  legendItems.call(arrangeLegendItems);
  legendItems.enter().append('g').call(arrangeLegendItems);
  legendItems.exit().remove();

  function arrangeLegendItems(selection) {
    selection.each(function(d, i) {
      var elements = d3.select(this);
      var tData = d;

      elements.attr('class', 'legend-item');
      var legendRect = elements.selectAll("rect.legend-rect");
      if(legendRect.empty()) {
        legendRect = elements.append('rect').attr('class', 'legend-rect');
      }
      legendRect.transition().duration(config.transitionDuration)
      .attr('x', config.dimensions.left)
      .attr('width', rectWidth)
      .attr('height', rectHeight)
      .attr('fill', config.colors(d))
      .attr('y', legendScale(tData) + config.dimensions.top);

      var legendText = elements.selectAll("text.legend-text");
      if(legendText.empty()) {
        legendText = elements.append('text').attr('class', 'legend-text');
      }
      legendText.transition().duration(config.transitionDuration)
      .text(function(d) {
        if(tData.length > 15)
          return tData.substring(0,15)+'...';
        return tData;
      })
      .attr('x', config.dimensions.left + rectWidth + rectHeight)
      .attr('y', legendScale(tData) + config.fontSize*1.3 + config.dimensions.top);
    });
  }
}
