//replace this with a request to data/data.json
var data = {
  "name": "Quartely Top Destinations",
	"data": [{
		"x": "Q1",
    "y": 18000,
    "z": "Los Angeles",
	},{
		"x": "Q2",
    "y": 19000,
    "z": "Los Angeles",
	},{
		"x": "Q3",
    "y": 20000,
    "z": "Los Angeles",
	},{
		"x": "Q4",
    "y": 21000,
    "z": "Los Angeles",
	},{
		"x": "Q1",
    "y": 11000,
    "z": "New York City",
	},{
		"x": "Q2",
    "y": 12000,
    "z": "New York City",
	},{
		"x": "Q3",
    "y": 14000,
    "z": "New York City",
	},{
		"x": "Q4",
    "y": 13000,
    "z": "New York City",
	},{
		"x": "Q1",
    "y": 39000,
    "z": "San Francisco",
	},{
		"x": "Q2",
    "y": 23000,
    "z": "San Francisco",
	},{
		"x": "Q3",
    "y": 33000,
    "z": "San Francisco",
	},{
		"x": "Q4",
    "y": 34000,
    "z": "San Francisco",
	}],
	"units": "$"
};

document.addEventListener("DOMContentLoaded", function(event) {
  //create chart object
  var chart = barMultiVertical2();

  //select element to attach it to
  var el = d3.select(".chart");

  //create chart in element
  chart(el);

  //set dimensions
  chart.config({height: 200, width:600, colors:d3.schemeCategory20});

  //give data
  chart.data(data);

  //call refresh to redraw
  chart.refresh();
});
